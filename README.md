# README #
Represent Employee hierarchy in an organization.

### Technology Stack
- Java 8
- SpringBoot
- Angular 2
- Mockito, JUnit
- Maven

### Assumptions 
1) CEO position is held by a single Employee and is at the top of the Employee Tree (i.e. only one employee with Manager ID: null/empty)
2) Employee cannot be his own manager.
Note: The tree will appear only when a CEO employee is inserted in the database.

### Data Structure
The Employees are arranged in an N-ary Tree structure, with CEO (single node) in the top. 
The nodes represent the Employee (data)

	Employee: 
		Employee ID: Number
		Name: String
		Manager ID: Number

	Node
	 _______________________
	|						|
	| Data <Employee>		|
	|						|
	| List<Node> children	|
	|_______________________|
	
	Tree ________				Hash Map (for indexing the nodes in the tree, useful when inserting a new Employee)
		| Root   |					 _______________________________________	
		|	node |					|	1	|	2	|	3	|....			|
		|__id 1__|	<-----------|	|_______|_______|_______|_______________|	
			|					|---|_______|_______|_______|...____________|
			|		 ___________________________|		|
		 _______<----|	________						|
		|Child	|	   |Child	|<----------------------
		|Node 1 | ---> |Node 2	| ...
		|_id 2__|	   |_id 3 __|
			

### To run
> mvn spring-boot:run

#### Program Structure

- src
	- main
		- java (Backend)
			 -EmployeeHierarchyApplication.java
			 -RestConfig.java
			 -model/
					-Tree.java
					-Node.java
					-Employee.java
			 -repo/
					-EmployeeId.java
					-EmployeeRepository.java 
			 -generic/
					-Constants.java
			 -exceptions/
					-InvalidEmployeeException.java
					-EmployeeAlreadyExistsException.java
					-InvalidManagerException.java

			 -service/
					-EmployeeService.java
			 -controller/
					-EmployeeController.java
		- resources
			
		- webclient (Frontend)
			src/
				-app/
					-app.component.spec.ts
					-app.component.css
					-app.module.ts
					-app.service.ts
					-app.component.ts
					-app.component.html
					-TreeNode.ts - Model to represent the tree node
					-Employee.ts - Model to represent the Employee.

					-tree-view/
						 tree-view.component.spec.ts
						 tree-view.component.css
						 tree-view.component.ts
						 tree-view.component.html

					-add-employee/
						 add-employee.component.spec.ts
						 add-employee.component.css
						 add-employee.component.ts
						 add-employee.component.html
			
	- test



			
