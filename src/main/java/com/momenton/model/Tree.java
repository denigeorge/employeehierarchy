package com.momenton.model;

/**
 * Created by denigeorge on 8/14/17.
 * The employee structure is an n-ary tree. Assumption Only one employee at the top of the
 * ladder that is CEO.
 */
public class Tree<T> {

    private Node<T> root;

    public Tree(){

    }

    public Tree(Node<T> root) {
        this.root = root;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public Node<T> getRoot() {
        return root;
    }

    public void setRoot(Node<T> root) {
        this.root = root;
    }

}
