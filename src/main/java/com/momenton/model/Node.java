package com.momenton.model;

/**
 * Created by denigeorge on 8/14/17.
 * Represents an Employee in the Organisation structure.
 * Contains the list of employees that report directly to this employee.
 */
import java.util.ArrayList;
import java.util.List;

public class Node<T> {

    private T data;
    private List<Node<T>> children;

    public Node(T data) {
        this.data = data;
        this.children = null;
    }

    public void addChild(Node<T> child) {
        if (this.children == null){
            this.children = new ArrayList<>();
        }
        children.add(child);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<Node<T>> getChildren() {
        return children;
    }

    public void setChildren(List<Node<T>> children) {
        this.children = children;
    }

}
