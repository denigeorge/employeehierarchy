package com.momenton.model;

import javax.persistence.*;

/**
 * Created by denigeorge on 8/14/17.
 *
 * Entity class for Employee as stored in the database.
 */
@Entity
public class Employee {

    @Id
    @Column(name = "emp_id")
    private Integer empId;

    @Column(name = "name")
    private String name;

    @Column(name = "manager_id")
    private Integer managerId;


    public Employee() {
    }
    public Employee(Integer empId, String name, Integer managerId) {
        this.empId = empId;
        this.name = name;
        this.managerId = managerId;
    }
    public Integer getEmpId() {
        return empId;
    }
    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getManagerId() {
        return managerId;
    }
    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }
}