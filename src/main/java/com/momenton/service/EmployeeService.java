package com.momenton.service;

/**
 * Created by deni george on 8/14/17.
 * Service Class to perform the activities
 * 1) Creating the tree representation of the Employees.
 * 2) Adding a new Employee
 * 3) retrieving the CEO.
 */

import com.momenton.exceptions.EmployeeAlreadyExistsException;
import com.momenton.exceptions.InvalidEmployeeException;
import com.momenton.exceptions.InvalidManagerException;
import com.momenton.generic.Constants;
import com.momenton.model.Employee;
import com.momenton.model.Node;
import com.momenton.model.Tree;
import com.momenton.repo.EmployeeId;
import com.momenton.repo.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    private Tree<Employee> employeeTree;

    private HashMap<Integer, Node<Employee>> employeeMap;

    public Tree<Employee> getEmployeeTree() {
        return employeeTree;
    }

    public HashMap<Integer, Node<Employee>> getEmployeeMap() {
        return employeeMap;
    }

    @PostConstruct
    public void init() {
        reconstructTree();
    }

    /*
     * Construct the Employee tree.
     */
    public void reconstructTree() {

        employeeMap = new HashMap<>();
        employeeTree = new Tree<>();

        List<Employee> ceos = employeeRepository.findByManagerIdIsNull();

        // assuming there is a single CEO.
        if (ceos.isEmpty() || (ceos.size() > 1)) {
            return;
        }

        Employee ceo = ceos.get(0);

        if (ceo != null) {

            Node<Employee> root = new Node<Employee>(ceo);
            employeeTree.setRoot(root);
            employeeMap.put(ceo.getEmpId(), root);

            Queue<Node<Employee>> empQueue = new LinkedList<>();

            empQueue.add(root);

            while (!empQueue.isEmpty()) {
                Node<Employee> e = empQueue.remove();
                Integer empId = e.getData().getEmpId();
                List<Employee> subordinates = employeeRepository.findByManagerId(empId);

                if (!subordinates.isEmpty()) {
                    List<Node<Employee>> subordinateNodes = subordinates.stream().map((a) -> {
                        Node<Employee> empNode = new Node<>(a);
                        employeeMap.put(a.getEmpId(), empNode);
                        return empNode;
                    }).collect(Collectors.toList());

                    subordinateNodes.stream().forEach((a) -> e.addChild(a));
                    empQueue.addAll(subordinateNodes);
                }
            }
        }
    }

    public Employee getCeoEmployee() {
        List<Employee> ceos = employeeRepository.findByManagerIdIsNull();

        if (ceos.isEmpty() || (ceos.size() > 1)) {
            return null;
        }
        return ceos.get(0);

    }

    public Integer saveEmployee(Employee employee) throws InvalidEmployeeException, InvalidManagerException, EmployeeAlreadyExistsException {

        if (employee == null) {
            throw new InvalidEmployeeException();
        }

        if (employee.getEmpId() == null)
            throw new InvalidEmployeeException();

        if (employee.getName() == null || employee.getName().trim().length() == 0)
            employee.setName(Constants.TEMPORARY_EMPLOYEE);

        Integer empId = employee.getEmpId();
        Integer managerId = employee.getManagerId();

        // Employee ID and the manager ID is the same.
        if (empId.equals(managerId))
            throw new InvalidEmployeeException();

        Employee e = employeeRepository.findOne(empId);

        if (e != null) {
            throw new EmployeeAlreadyExistsException();
        }

        if (managerId != null) {
            // check whether the manager is properly assigned.
            Employee managerEmp = employeeRepository.findOne(managerId);
            if (managerEmp == null)
                throw new InvalidManagerException();
        }

        if (managerId != null) {
            employeeRepository.save(employee);
            if (!employeeTree.isEmpty()) {
                Node<Employee> empNode = new Node<Employee>(employee);
                employeeMap.get(managerId).addChild(empNode);
                employeeMap.put(empId, empNode);
            } else {
                return Constants.OK;
            }
        } else if (managerId == null || managerId.equals(0)) {
            // in case the manager id is not available.
            // check if this is the CEO employee.
            if (employeeTree.isEmpty()) {
                employeeRepository.save(employee);
                reconstructTree();
            } else {
                // this is an erroneous case as CEO is already there
                // and attempt is made to add an employee without manager.
                throw new InvalidManagerException();
            }
        }

        return empId;
    }

    public List<Integer> getEmployeeIds() {
        List<EmployeeId> employeeIds = employeeRepository.findAllByEmpIdNotNull();
        return employeeIds.stream().map((a) -> a.getEmpId()).collect(Collectors.toList());
    }

}