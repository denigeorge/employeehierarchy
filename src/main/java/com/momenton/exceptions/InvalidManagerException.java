package com.momenton.exceptions;

/**
 * Created by denigeorge on 8/16/17.
 */
public class InvalidManagerException extends Exception{
    private static final String message = "Invalid Manager Specified.";

    public InvalidManagerException(){
        super(message);
    }
}
