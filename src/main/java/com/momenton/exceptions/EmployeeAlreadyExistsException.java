package com.momenton.exceptions;

/**
 * Created by denigeorge on 8/16/17.
 */
public class EmployeeAlreadyExistsException extends Exception {
    private static final String message = "Employee Already Exists.";

    public EmployeeAlreadyExistsException() {
        super(message);
    }
}
