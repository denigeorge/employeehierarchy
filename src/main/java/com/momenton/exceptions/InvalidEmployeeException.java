package com.momenton.exceptions;

/**
 * Created by denigeorge on 8/16/17.
 */
public class InvalidEmployeeException extends Exception{
    private static final String message = "Invalid Employee.";

    public InvalidEmployeeException(){
        super(message);
    }
}
