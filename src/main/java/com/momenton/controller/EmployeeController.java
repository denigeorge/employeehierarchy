package com.momenton.controller;

/**
 * Created by denigeorge on 8/14/17.
 */

import com.momenton.exceptions.EmployeeAlreadyExistsException;
import com.momenton.exceptions.InvalidEmployeeException;
import com.momenton.exceptions.InvalidManagerException;
import com.momenton.generic.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.momenton.model.Employee;
import com.momenton.model.Node;
import com.momenton.service.EmployeeService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/getceo", produces = MediaType.APPLICATION_JSON_VALUE)
    public Employee getRootEmployee() {
        Employee emp = employeeService.getCeoEmployee();
        return emp;
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Node<Employee>> getEmployees() {

        if (employeeService.getEmployeeTree().isEmpty()) {
            return null;
        }

        Node<Employee> rootNode = employeeService.getEmployeeTree().getRoot();
        return new ResponseEntity<Node<Employee>>(rootNode, HttpStatus.OK);
    }

    @GetMapping(value = "/listempids", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Integer> getEmployeeIds() {
        return employeeService.getEmployeeIds();
    }

    @PostMapping(value = "/saveemployee", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveEmployee(@RequestBody Employee emp) {

        try {
            employeeService.saveEmployee(emp);
        } catch (InvalidEmployeeException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.NOT_ACCEPTABLE);
        } catch (InvalidManagerException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.NOT_ACCEPTABLE);
        } catch (EmployeeAlreadyExistsException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity<>("Employee added successfully.", HttpStatus.OK);
    }


    @GetMapping(value = "/listsample", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getSampleEmployees() {
        Employee rootEmp = new Employee(0, "emp1", 0);
        Node<Employee> rootNode = new Node<Employee>(rootEmp);

        Node<Employee> node1 = new Node<Employee>(new Employee(1, "emp2", 0));
        Node<Employee> node2 = new Node<Employee>(new Employee(2, "emp3", 0));


        Node<Employee> node3 = new Node<Employee>(new Employee(3, "emp4", 1));
        Node<Employee> node4 = new Node<Employee>(new Employee(4, "emp5", 1));

        node1.addChild(node3);
        node1.addChild(node4);

        Node<Employee> node5 = new Node<Employee>(new Employee(5, "emp6", 2));
        Node<Employee> node6 = new Node<Employee>(new Employee(6, "emp7", 2));
        Node<Employee> node7 = new Node<Employee>(new Employee(6, "emp8", 2));
        node2.addChild(node5);
        node2.addChild(node6);
        node2.addChild(node7);

        rootNode.addChild(node1);
        rootNode.addChild(node2);

        String json = "";

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            json = ow.writeValueAsString(rootNode);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;

    }

}
