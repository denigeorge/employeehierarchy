package com.momenton.generic;

/**
 * Created by denigeorge on 8/15/17.
 */
public interface Constants {
    public static final Integer OK = -1; // to avoid a situation where the employee ID is 0.
    public static final Integer EMP_ALREADY_EXISITING = -2;
    public static final Integer INVALID_EMP_ID = -3;
    public static final Integer MANAGER_NOT_SPECIFIED = -4;
    public static final String TEMPORARY_EMPLOYEE = "Temporary Employee";
}
