package com.momenton.repo;

/**
 * Created by denigeorge on 8/14/17.
 * For select column, getting all the employee IDs.
 */
public interface EmployeeId{
    Integer getEmpId();
}