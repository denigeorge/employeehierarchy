package com.momenton.repo;

/**
 * Created by denigeorge on 8/14/17.
 * Repository interface for Employee Entity.
 */
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.momenton.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    List<Employee> findByManagerId(int managerId);
    List<Employee> findByManagerIdIsNull();
    List<EmployeeId> findAllByEmpIdNotNull();
}
