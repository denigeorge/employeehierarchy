import { Component } from '@angular/core';
import {TreeNode} from "./TreeNode";
import {AppService} from "./app.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public employeetree: TreeNode;

  constructor(private empService: AppService) { }

  public refreshTree() {
    this.empService.getEmployeeTree().subscribe(
      (response) => {
        this.employeetree = response.json() || { };
        console.log(this.employeetree);
      }, error => console.log()
    );
  }

  ngOnInit() {
    this.refreshTree();
  }
}
