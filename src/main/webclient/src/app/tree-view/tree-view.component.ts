import {Component, Input, OnInit} from '@angular/core';
import {TreeNode} from '../TreeNode';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent implements OnInit {

  constructor() { }

  @Input() data: TreeNode;

  ngOnInit() {
  }

}
