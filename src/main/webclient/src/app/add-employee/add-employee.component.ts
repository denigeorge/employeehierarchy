import { Component, OnInit } from '@angular/core';
import {AppService} from "../app.service";
import {Employee} from "../Employee";
import {AppComponent} from "../app.component";

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  public name: string;
  public empId: number;
  public managerId: number;

  public message: string;
  public messageClass: string;

  public employee: Employee;

  constructor(private empService: AppService, private appComponent: AppComponent) { }

  onAddEmployee(){
    this.employee = new Employee(this.name, this.empId, this.managerId);
    this.empService.saveEmployee(this.employee)
      .subscribe((response) => {
            console.log(response);
            this.message = response.text();
            this.messageClass = "alert-success";
            this.appComponent.refreshTree();
          }
      ,
          (error) => {
            console.log(error);
            this.message = error.text();
            this.messageClass = "alert-danger"
          }
      );
  }

  ngOnInit() {

  }

}
