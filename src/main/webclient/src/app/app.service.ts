import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Employee} from "./Employee";

@Injectable()
export class AppService {
  constructor(private http: Http) {}
  getEmployeeTree() {
    return this.http.get('http://localhost:8080/api/list');
  }

  saveEmployee(employee: Employee){
    return this.http.post('http://localhost:8080/api/saveemployee', employee);
  }
}
