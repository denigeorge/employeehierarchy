/**
 * Created by denigeorge on 8/14/17.
 */

export class TreeNode {
  constructor(public data: {
                name: string,
                empId: number,
                managerId: number
              },
              public children: Array<TreeNode>) {

  }
}
