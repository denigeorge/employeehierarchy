package com.momenton.service;

import com.momenton.exceptions.EmployeeAlreadyExistsException;
import com.momenton.exceptions.InvalidEmployeeException;
import com.momenton.exceptions.InvalidManagerException;
import com.momenton.model.Employee;
import com.momenton.repo.EmployeeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.DirtiesContext;

import static org.mockito.Mockito.when;

/**
 * Created by denigeorge on 8/16/17.
 */
@DirtiesContext
public class EmployeeServiceTest {
    @InjectMocks
    private EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = EmployeeAlreadyExistsException.class)
    public void testSaveSameEmployee() throws EmployeeAlreadyExistsException, InvalidManagerException, InvalidEmployeeException {
        Employee e2 = new Employee(100, "test", 10);
        when(employeeRepository.findOne(e2.getEmpId())).thenReturn(e2);
        employeeService.saveEmployee(e2);
    }

    @Test(expected = InvalidEmployeeException.class)
    public void testInvalidEmployeeSave() throws EmployeeAlreadyExistsException, InvalidManagerException, InvalidEmployeeException {
        Employee e = new Employee(null, "test", null);
        employeeService.saveEmployee(e);
    }

    @Test(expected = InvalidEmployeeException.class)
    public void empIdAndManagerIdSame() throws EmployeeAlreadyExistsException, InvalidManagerException, InvalidEmployeeException {
        Employee e = new Employee(100,"test", 100);
        employeeService.saveEmployee(e);
    }

}
