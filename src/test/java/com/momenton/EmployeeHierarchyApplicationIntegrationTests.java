package com.momenton;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.momenton.model.Employee;
import com.momenton.model.Node;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.eventFrom;
import static org.hamcrest.Matchers.instanceOf;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeHierarchyApplication.class)
@WebAppConfiguration
public class EmployeeHierarchyApplicationIntegrationTests {


    @Test
    public void testGETEmployeeTree() throws IOException {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8080/api/saveemployee",
                new Employee(1, "Ceo Emp", null), String.class);
        responseEntity = restTemplate.postForEntity("http://localhost:8080/api/saveemployee",
                new Employee(2, "First Emp", 1), String.class);
        responseEntity = restTemplate.postForEntity("http://localhost:8080/api/saveemployee",
                new Employee(3, "Second Emp", 1), String.class);

        responseEntity = restTemplate.getForEntity("http://localhost:8080/api/list", String.class);

        Employee ceo = new Employee(1, "Ceo Emp", null);
        Employee firstEmp = new Employee(2, "First Emp", 1);
        Employee secondEmp = new Employee(1, "Second Emp", 1);

        Node<Employee> ceoNode = new Node<>(ceo);
        Node<Employee> firstEmpNode = new Node<>(firstEmp);
        Node<Employee> secondEmpNode = new Node<>(secondEmp);

        ceoNode.addChild(firstEmpNode);
        ceoNode.addChild(secondEmpNode);

        assertThat(responseEntity.getBody(), equalTo("{\"data\":{\"empId\":1,\"name\":\"Ceo Emp\",\"managerId\":null},\"children\":[{\"data\":{\"empId\":2,\"name\":\"First Emp\",\"managerId\":1},\"children\":null},{\"data\":{\"empId\":3,\"name\":\"Second Emp\",\"managerId\":1},\"children\":null}]}"));
    }

    @Test
    public void testPOSTEmployee() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8080/api/saveemployee",
                new Employee(2, "Adding Again", 1), String.class);

        ResponseEntity<?> okRes = new ResponseEntity<>("Employee Already Exists.", HttpStatus.NOT_ACCEPTABLE);

        assertThat(responseEntity.getBody(), equalTo(okRes.getBody()));
        assertThat(responseEntity.getStatusCode(), equalTo(okRes.getStatusCode()));
    }


}
