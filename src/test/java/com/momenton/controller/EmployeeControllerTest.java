package com.momenton.controller;

import com.momenton.exceptions.EmployeeAlreadyExistsException;
import com.momenton.exceptions.InvalidEmployeeException;
import com.momenton.exceptions.InvalidManagerException;
import com.momenton.generic.Constants;
import com.momenton.model.Employee;
import com.momenton.repo.EmployeeRepository;
import com.momenton.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by deni george on 8/14/17.
 */
@DirtiesContext
public class EmployeeControllerTest {

    @InjectMocks
    private EmployeeController employeeController;

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private EmployeeService employeeService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSaveEmployee() throws InvalidManagerException, InvalidEmployeeException, EmployeeAlreadyExistsException {
        Employee e = new Employee(100, "test", 10);

        when(employeeService.saveEmployee(e)).thenReturn(Constants.OK);

        ResponseEntity<?> re = employeeController.saveEmployee(e);
        ResponseEntity<?> okRes = new ResponseEntity<>("Employee added successfully.", HttpStatus.OK);
        assertThat(re, is(okRes));
    }

    @Test
    public void testSaveInvalidEmployee() throws InvalidManagerException, InvalidEmployeeException, EmployeeAlreadyExistsException {
        Employee e = new Employee(null, "test", 10);

        when(employeeService.saveEmployee(e)).thenThrow(new InvalidEmployeeException());

        ResponseEntity<?> re = employeeController.saveEmployee(e);
        ResponseEntity<?> invalidRes = new ResponseEntity<>("Invalid Employee.", HttpStatus.NOT_ACCEPTABLE);
        assertThat(re, is(invalidRes));
    }

    @Test
    public void testSaveSameEmployee() throws EmployeeAlreadyExistsException, InvalidManagerException, InvalidEmployeeException {
        Employee e = new Employee(100, "test", 10);

        when(employeeService.saveEmployee(e)).thenThrow(new EmployeeAlreadyExistsException());

        ResponseEntity<?> alreadyExistsEmp = new ResponseEntity<>("Employee Already Exists.", HttpStatus.NOT_ACCEPTABLE);
        ResponseEntity<?> re = employeeController.saveEmployee(e);
        assertThat(re, is(alreadyExistsEmp));
    }

}
